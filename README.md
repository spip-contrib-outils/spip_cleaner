## Introduction

Ce script a pour objectif d'aider à nettoyer des sites SPIP corrompus. Pour ce faire le script:
 * scanne une arborescence serveur (par défaut **/var/www/**)
 * détecte les racines contenant un site SPIP
 * supprime des fichiers selon une liste d'exclusion définie (indiquée et considérée par la communauté non licite)
 * détecte certaines signatures de fichiers à contrôler manuellement
 * restaure spip.php, spip_loader.php en version officielle
 * installe l'ecran de sécurité à jour

## /!\ ATTENTIION /!\

Ce script ne fait pas de magie. Il accompagne les devops/adminsys pour nettoyer en masse les sites vérolés. Des actions complémentaires sont obligatoirement à faire ensuite pour garantir qu'un site soit intégralement nettoyé.
Sans être exhaustif on peut noter les actions suivantes :
* mettre à jour les accès aux bases de données
* mettre à jour les identifiants utilisateurs
* contrôler la "qualité" des données en base (injection de code, ....)
* contrôler la "qualité" du core SPIP (réinstallation depuis une source fiable)
* contrôler la "qualité" des plugins (réinstallation depuis une source fiable)
*  ...

## Usage

```./clean_spip.sh [-p|--path /var/www] [-d||--dry-run 1] [-v |--verbose] [-h|--help]```

Tous les arguements sont optionnels.
Les arguments disponibles sont :
  * **-h** ou **--help** donne les indications d'usage sur le script
  * **-p** ou **--path** indique le répertoire à contrôler, par défaut il s'agit de */var/www/*. Le chemin doit exister.
  * **-d** ou **--dry-run** indique si on exécute ou non les actions de purge, par défaut *1*. Les valeurs attendues sont 0 ou 1
  * **-v** ou **--verbose** donne plus de détail sur les actions exécutables ou à exécuter. Donne également la liste des fichiers à contrôler.

## Comportement

Le script par défaut ne fait rien que de lister ce qui est possible de faire. Il est possible de modifier son comportement selon les options explicitées dans le paragraphe USAGE.
Le script fonctionne en **dry-run** par défaut. Aucune action de suppession n'est réalisée par défaut. 
Pour lancer la suppression effective des fichiers, il faut l'expliciter déclarant l'argument **dry-run** à 0.
