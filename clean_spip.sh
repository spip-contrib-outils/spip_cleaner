#!/bin/bash

##
# Version 1.0
#   version utilisable en production
#   mode dry-run plus complet
#   mode verbose pour donner plus d'information sur les fichiers vérolés et les actions réalisées
#   utilisation de motif de recherche par expression régulière
# Version 0.5
#  argument pour le path de départ
#  chercher tous les spip (exlusion de chemin faux positif comme plugins-dist/ et ecrire/auth/
#  considérer les sites vérolés sur un grep dans les path spip identifiés
# Version 0.4
# Version 0.3
#  Ajout ecran de securité
# Version 0.2
#  ajout dry-run
#  check cyclique avec liste des spip identifié automatiquement
#  support coloration message
# Version 0.1
#  Premier jet
##

## Requirement to run this scrip
### util-linux is required to take in consideration bash option
getopt -T >/dev/null 2>&1
if (($? != 4)); then
    echo "util-linux getopt is not installed.  Aborting." >&2
    exit 2
fi

##
usage() {
    echo "Usage:"
    echo " ${0} [ --path=<path_to_check> | --path <path_to_check> | -p <path_to_check> ] [ -d <0|1> | --dry-run <0|1> ] [ --verbose | -v ] [ -l | --list ]"
    echo " ${0} [ --help | -h ]"
    echo
    echo "Default path is /var/www/ "
    echo "Dry-run is enabled by default"
    echo "verbose is disabled by default"
    echo "list is include in verbose, list display all file detected as corrupted"
}

## Default value
global_dry_run=1
global_www_dir="/var/www/"
global_verbose=0
global_list=0

## Internal value
declare -a global_crontab_users
declare -a global_spip_path_founds
declare -a global_wp_path_founds
declare -a global_spip_hacked

##Patterns
patterns_file_type_to_check=(
  ELF\ 32
  ELF\ 64
)

patterns_content_to_check=(
  "@?eval\(@?base64_decode"
  "@?encode\(@?base64_decode"
  "error_reporting\(0\);.*\\n?.*set_time_limit\(0\);"
  "ini_set\(\"\\\""
  "@eval\(\$\_"
  "'oogle'"
  "已"
  "RunIcmp2"
  "_vsx"
  "make_link"
)

patterns_filename_tocheck=(
  sbin/.*
  bin/.*
  ecrire/auto\.php
  ecrire/local\.php
  ecrire/manual\.php
  ecrire/\.rnd
  ecrire/manual\.php
  IMG/\.about\.php
  lib/api\.php
  lib/api_response\.php
  lib/api_request\.php
  lib/langs\.php
  local/config\.php
  local/define\.php
  local/undefine\.php
  plugins/auto/saisies/saisies\.php
  plugins/auto/saisies/saisies_loader\.php
  plugins/auto/saisies/saisies_unloader\.php
  plugins/plugins\.php
  sys/.*
  request\.php
  response\.php
  update\.php
  data\.php
  .*x86
  .*x86_64
  hp_sniff.py\.*
  InUdrHFO\.php
  tst.py
  xmrig.*
  6diSibAbzNC4qNywNvKPq7IfcfxdyLvP7trJYdXG
  killer.*py
  mine.*
  spip-upgrade\.php
  [a-zA-Z]\.php
  [a-zA-Z][a-zA-Z]\.php
  [0-9]\.php
  [0-9]\.txt
  [a-zA-Z]*
  .*funs.php
  classsmtps.php
  comfunctions.php
  delpaths.php
  doiconvs.php
  epinyins.php
  export.php
  gdftps.php
  inputs.php
  phpzipincs.php
  siteheads.php
  termps.php
  thoms.php
  txets.php
  *.so
  pwn
  verc\.ini
  xpl\.sh
)

## Took from http://mywiki.wooledge.org/ComplexOptionParsing#getopts_long_option_trickery
i=$(($# + 1)) # index of the first non-existing argument
declare -A longoptspec
# Use associative array to declare how many arguments a long option
# expects. In this case we declare that loglevel expects/has one
# argument and range has two. Long options that aren't listed in this
# way will have zero arguments by default.
longoptspec=( [path]=1 [dry-run]=1 )
optspec=":d:p:h-:v-:l-:"
while getopts "${optspec}" opt; do
while true; do
    case "${opt}" in
        -) #OPTARG is name-of-long-option or name-of-long-option=value
            if [[ ${OPTARG} =~ .*=.* ]] # with this --key=value format only one argument is possible
            then
                opt=${OPTARG/=*/}
                ((${#opt} <= 1)) && {
                    echo "Syntax error: Invalid long option '${opt}'" >&2
                    exit 2
                }
                if ((longoptspec[\$opt] != 1))
                then
                    echo "Syntax error: Option '${opt}' does not support this syntax." >&2
                    exit 2
                fi
                OPTARG=${OPTARG#*=}
            else #with this --key value1 value2 format multiple arguments are possible
                opt="${OPTARG}"
                ((${#opt} <= 1)) && {
                    echo "Syntax error: Invalid long option '${opt}'" >&2
                    exit 2
                }
                OPTARG=("${@:OPTIND:longoptspec[\$opt]}")
                ((OPTIND+=longoptspec[\$opt]))
                ((OPTIND > i)) && {
                    echo "Syntax error: Not all required arguments for option '${opt}' are given." >&2
                    exit 3
                }
            fi

            continue #now that opt/OPTARG are set we can process them as
            # if getopts would've given us long options
            ;;
        p|path)
            if [[ -d "${OPTARG}" ]]; then
              global_www_dir=${OPTARG}
            else
              echo "Syntax error: path option must be a valid directory" >&2
              exit 2
            fi
            ;;
        d|dry-run)
            if [[ "${OPTARG}" == "0" || "${OPTARG}" == "1" ]]; then
              global_dry_run=${OPTARG}
            else
              echo "Syntax error: dry-run option is only 0 or 1" >&2
              exit 2
            fi
            ;;
        h|help)
            usage
            exit 0
            ;;
        l|list)
            global_list=1
            ;;
        v|verbose)
            global_verbose=1
            global_list=1
            ;;
        ?)
            echo "Syntax error: Unknown short option '${OPTARG}'" >&2
            exit 2
            ;;
        *)
            echo "Syntax error: Unknown long option '${opt}'" >&2
            exit 2
            ;;
    esac
break; done
done

if [[ -n ${!OPTIND-} ]]; then
        echo "Syntax error: Unknown option '${!OPTIND-}'" >&2
        exit 2
fi

## Protect configuration value
readonly global_dry_run
readonly global_www_dir
readonly global_verbose
readonly patterns_file_type_to_check
readonly patterns_content_to_check
readonly patterns_filename_tocheck

execute_cmd() {
    local cmd_str

    if [[ ! -t 0 ]]
    then
        cat
    fi
    if [[ ${global_dry_run} == 1 ]]; then
      if [[ ${global_verbose} == 1 ]]; then
        printf -v cmd_str '%s ' "$@"
        vprint "    DRYRUN: ${cmd_str}" "WHITE"
      fi
    else
      "$@"
      if [[ ${global_verbose} == 1 ]]; then
        printf -v cmd_str '%s ' "$@"
        vprint "    RUN: ${cmd_str}" "WHITE"
      fi
   fi
}

function vprint() {
  local msg
  local color
  local NC
  local BLACK
  local RED
  local GREEN
  local YELLOW
  local BLUE
  local PURPLE
  local CYAN
  local WHITE

  msg=$1
  color=$2

  if [[ -z "$color" ]];then
    color="WHITE"
  fi

  # Reset
  NC='\033[0m'       # Text Reset
  # Regular Colors
  # shellcheck disable=SC2034
  BLACK='\033[0;30m'        # Black
  # shellcheck disable=SC2034
  RED='\033[0;31m'          # Red
  # shellcheck disable=SC2034
  GREEN='\033[0;32m'        # Green
  # shellcheck disable=SC2034
  YELLOW='\033[0;33m'       # Yellow
  # shellcheck disable=SC2034
  BLUE='\033[0;34m'         # Blue
  # shellcheck disable=SC2034
  PURPLE='\033[0;35m'       # Purple
  # shellcheck disable=SC2034
  CYAN='\033[0;36m'         # Cyan
  # shellcheck disable=SC2034
  WHITE='\033[0;37m'        # White

  printf "${!color}%s${NC}\n" "${msg}"
}

function search_corrupted_file_type_pattern() {
  local spip_dir
  local files_contain_pattern

  spip_dir="${1}"

  for pattern in "${patterns_file_type_to_check[@]}";do
    files_contain_pattern=$(find "${spip_dir}" -type f -exec file --exclude ascii {} \; | grep -i "${pattern}" | cut -d : -f 1 )
    if [[ -n ${files_contain_pattern} ]]; then
      vprint "Some files have ${pattern} pattern type file" "RED"
      if [[ ${global_list} == 1 ]]; then
        for file in ${files_contain_pattern};do
          vprint "    ${file}" "WHITE"
        done;
      fi
      if [[ $(echo "${global_spip_hacked[@]}" | grep -ow "${spip_dir}" | wc -w) -lt 1 ]]; then
        global_spip_hacked+=("${spip_dir}")
      fi
    fi
  done;

}

function search_corrupted_pattern() {
  local spip_dir
  local pattern
  local files_contain_pattern

  spip_dir="${1}"

  for pattern in "${patterns_content_to_check[@]}";do
    files_contain_pattern=$(grep -Ezlr "${pattern}" "${spip_dir}")
    if [[ -n ${files_contain_pattern} ]]; then
      vprint "Some files have ${pattern} pattern" "RED"
      if [[ ${global_list} == 1 ]]; then
        for file in ${files_contain_pattern};do
          vprint "    ${file}" "WHITE"
        done;
      fi
      if [[ $(echo "${global_spip_hacked[@]}" | grep -ow "${spip_dir}" | wc -w) -lt 1 ]]; then
        global_spip_hacked+=("${spip_dir}")
      fi
    fi
  done;
}

function clean_wp() {
    wp_dir="${1}"

    search_corrupted_files "${wp_dir}"
    search_corrupted_file_type_pattern "${wp_dir}"
}

function clean_spip() {
  local spip_dir
  local spip_index_hacked

  spip_dir="${1}"
  spip_index_hacked=$(grep -q 'spip_pass' "${spip_dir}/spip.php")
  if [ "${spip_index_hacked}" ];then
    if [[ ${global_verbose} == 1 ]]; then
      vprint "    ${spip_index_hacked} looks hacked" "WHITE"
    fi
    global_spip_hacked+=("${spip_dir}")
  fi

  execute_cmd find "${spip_dir}" -name '*.cache' -delete -print
  execute_cmd rm -r "${spip_dir}/tmp/"
  execute_cmd mkdir -p "${spip_dir}/tmp"
  execute_cmd rm -r "${spip_dir}/local/"
  execute_cmd mkdir -p "${spip_dir}/local"
  execute_cmd wget https://git.spip.net/spip-contrib-outils/securite/raw/branch/master/ecran_securite.php -qO "${spip_dir}/config/ecran_securite.php"
  execute_cmd wget https://git.spip.net/spip/spip/raw/branch/master/spip.php -qO "${spip_dir}/spip.php"
  execute_cmd wget https://get.spip.net/spip_loader.php -qO "${spip_dir}/spip_loader.php"

  search_corrupted_files "${spip_dir}"
  search_corrupted_file_type_pattern "${spip_dir}"
}

function search_corrupted_files() {
  local spip_dir
  local files_with_pattern
  local file_with_pattern
  local file

  spip_dir=$1


  for file in "${patterns_filename_tocheck[@]}"; do
    #Check if files are present and notify about potential risk
    files_with_pattern=$(find "${spip_dir}" -regex "${spip_dir}/${file}$")
    if [[ -n ${files_with_pattern} ]]; then
      vprint "Some files with ${file} pattern name found" "RED"
      if [[ ${global_list} == 1 ]]; then
        for file_with_pattern in ${files_with_pattern};do
          vprint "    ${file_with_pattern}" "WHITE"
        done;
      fi
      if [[ $(echo "${global_spip_hacked[@]}" | grep -ow "${spip_dir}" | wc -w) -lt 1 ]]; then
        global_spip_hacked+=("${spip_dir}")
      fi
    fi
    execute_cmd find "${spip_dir}" -regex "${spip_dir}/${file}$" -delete -print
  done;
}

function check_crontab() {
  local spip_dir
  local user
  local crontab_content

  spip_dir=$1
  user=$(stat --printf %U "${spip_dir}")

  # Process only once each crontab
  if [[ $(echo "${global_crontab_users[@]}" | grep -ow "${user}" | wc -w) -lt 1 ]]; then
    global_crontab_users+=("${user}")
    if crontab_content=$(crontab -lu "${user}") ; then
      vprint "$user has a crontab to check" "RED"
    else
      vprint "$crontab_content" "WHITE"
    fi
  fi
}

function search_spip() {
  local www_dir
  local spip_files
  local spip
  local spip_full_path
  local spip_dir

  www_dir="${1}"
  spip_files=$(find "${www_dir}" -type d \( -path "*plugins-dist*" -o -path "*plugins*" -o -path "*ecrire/auth*" \) -prune -o -iname spip.php -print)
  for spip in ${spip_files};do
    spip_full_path=$(realpath "${spip}")
    spip_dir=$(dirname "${spip_full_path}")
    global_spip_path_founds+=("${spip_dir}")
  done
}

function search_wp() {
  local www_dir
  local wp_files
  local wp
  local wp_full_path
  local wp_dir

  www_dir="${1}"
  wp_files=$(find "${www_dir}" -type d \( -path "*wp-includes*" -o -path "*wp-content*" -o -path "*wp-admin*" \) -prune -o -iname wp-config.php -print)
  for wp in ${wp_files};do
    wp_full_path=$(realpath "${wp}")
    wp_dir=$(dirname "${wp_full_path}")
    global_wp_path_founds+=("${wp_dir}")
  done
}

if [[ ${global_dry_run} == 1 ]]; then
  vprint "Dry run enabled , action will not be executed" BLUE
else
  vprint "Dry run disabled , all action will be executed" RED
fi

vprint "Search SPIP in ${global_www_dir}" "BLUE"
search_spip "${global_www_dir}"

for spip in "${global_spip_path_founds[@]}"; do
  vprint "SPIP found : ${spip}" BLUE
  clean_spip "${spip}"
  search_corrupted_pattern "${spip}"
done;

vprint "Search WP in ${global_www_dir}" "BLUE"
search_wp "${global_www_dir}"

for wp in "${global_wp_path_founds[@]}"; do
  vprint "Wordpess found : ${wp}" BLUE
  clean_wp "${wp}"
  search_corrupted_pattern "${wp}"
done;

vprint "Search crontab execution" "BLUE"
for spip in "${global_spip_path_founds[@]}"; do
  check_crontab "${spip}"
done;

if [[ -n ${global_spip_hacked} ]]; then
  vprint "SPIP to check manualy" "RED"
  for spip in "${global_spip_hacked[@]}"; do
    vprint "${spip}" "WHITE"
  done;
else
  vprint "No SPIP alert detected" "GREEN"
fi